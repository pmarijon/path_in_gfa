import logging
import collections

import networkx as nx

from enum import Enum

from itertools import permutations

# global variable
def populate_graph(gfa, G): 
   
    is_contain = isContain(gfa)

    for d in gfa.dovetails:
        __add_dovetails(d, G, is_contain)
        __add_dovetails(d.complement(), G, is_contain)

def __add_dovetails(d, G, is_contain):
    G.add_node(d.from_name + d.from_orient, containment=is_contain(d.from_name))
    G.add_node(d.to_name + d.to_orient, containment=is_contain(d.to_name))

    if d.NM is None or d.om is None or d.om == 0:
        qual_val = 0.0
    else:
        qual_val = (d.NM / d.om) * 100.0

    G.add_edge(d.from_name + d.from_orient,
               d.to_name + d.to_orient,
               qual=qual_val,
               len=d.alignment.length_on_query(),
               overhang_len=d.to_segment.length - d.alignment.length_on_query(),
               overhang_maplen=d.get("om"),
               weight=1)

class isContain:
    
    def __init__(self, gfa):
        self.containment2pos = {l.to_name: i for i, l in enumerate(gfa.containments)}

    def __call__(self, node):
        return node in self.containment2pos

    def index(self, node):
        return self.containment2pos[node]

def populate_dag(G, DAG, firstNode, deep = -1):

    if deep == 0:
        return

    for successor in G.successors(firstNode):
        recurse = DAG.has_node(successor)

        DAG.add_edge(firstNode, successor, G.get_edge_data(firstNode, successor))
        if not nx.is_directed_acyclic_graph(DAG):
            DAG.remove_edge(firstNode, successor)

        if not recurse:
            populate_dag(G, DAG, successor, deep - 1)


def get_edge_in_len_order(G, v):
    for v, w, edge_len in sorted([(v, w, G.get_edge_data(v, w)["overhang_len"]) 
                                      for w in G.successors(v)], 
                                  key=lambda tup: tup[2]):
        yield (v, w, edge_len)


def transitive_reduction(G):
    FUZZ = 10

    edge_eliminate = set()
    inplay_node = set()

    for v in G.nodes():
        eliminated_node = set()
        
        for w in G.successors(v):
            inplay_node.add(w)
       
        if len(list(G.successors(v))) == 0:
            continue
        longest = max([data["overhang_len"] for v, w, data in G.edges(v, data=True)]) + FUZZ

        for _, w, vw_len in get_edge_in_len_order(G, v):
            for _, x, wx_len in get_edge_in_len_order(G, w):
                if vw_len + wx_len > longest:
                    break
 
                if x in inplay_node:
                    eliminated_node.add(x)

        for _, w, vw_len in get_edge_in_len_order(G, v): 
            nb_edge_inf_fuzz = 0
            for _, x, wx_len in get_edge_in_len_order(G, w):
                if wx_len > FUZZ:
                    break

                if x in inplay_node :
                    eliminated_node.add(x)
                    inplay_node.remove(x)
                    nb_edge_inf_fuzz +=1

            if nb_edge_inf_fuzz == 0:
                try:
                    x = next(get_edge_in_len_order(G,w))[1]
                    eliminated_node.add(x)
                except StopIteration:
                    # w have no successor
                    pass

        for w in G.successors(v):
            if w in eliminated_node:
                edge_eliminate.add((v, w))
                eliminated_node.remove(w)
            inplay_node.discard(w)

    [G.remove_edge(v, w) for v, w in edge_eliminate]
    
    return G

def generate_subgraph_union(firstNode, secondNode, graph, deep):

    shortest_path = nx.shortest_path(graph, firstNode, secondNode)
    edge_of_path = [i for i in zip(shortest_path, shortest_path[1:])]

    selected_node = set(shortest_path)
    for n in nx.dfs_successors(graph, source=firstNode, depth_limit=deep):
        selected_node.add(n)

    graph_reverse = nx.reverse(graph, copy=True)
    for n in nx.dfs_successors(graph_reverse, source=secondNode, depth_limit=deep):
        selected_node.add(n)

    return selected_node, edge_of_path


def linear_path(G, begin):
    exclude = set()

    if begin in exclude:
        return None, None


    # Search after
    path = list()
    path.append(begin)
    node = begin
    while "node have one successor and one predecessors":
        if node in exclude:
            break

        if len(set(G.successors(node))) > 1:
            break
        if len(set(G.predecessors(node))) > 1:
            break

        try:
            successor = next(G.successors(node))
        except StopIteration :
            break
       
        exclude.add(node)
        path.append(successor)
        node = successor

    # Search before
    node = begin
    while "node have one successor and one predecessors":
        if len(set(G.successors(node))) > 1:
            break
        if len(set(G.predecessors(node))) > 1:
            break

        try:
            predecessor = next(G.predecessors(node))
        except StopIteration :
            break
       
        exclude.add(node)
        path.insert(0, predecessor)
        node = predecessor

    # Check extremity of path
    if len(path) > 0:
        if len(set(G.successors(path[0]))) > 1:
            path.pop(0)
        elif len(set(G.predecessors(path[0]))) > 1:
            path.pop(0)

    if len(path) > 0:
        if len(set(G.successors(path[-1]))) > 1:
            path.pop()
        elif len(set(G.predecessors(path[-1]))) > 1:
            path.pop()
    
    return path

def linear_paths(G):
    exclude = set()

    paths = list()
    for node in G.nodes():
        if node in exclude:
            continue
        
        path = linear_path(G, node)
        if len(path) != 0:
            exclude.update(path)
            yield path

def contract_linear_path(G):
    H = G.copy()

    for i, path in enumerate(linear_paths(G)):
        if len(path) < 2:
            continue
        node_name = "_".join(path)
        H.add_node(node_name)

        for before in H.predecessors(path[0]):
            H.add_edge(before, node_name)

        for after in H.successors(path[-1]):
            H.add_edge(node_name, after)

        H.remove_nodes_from(path)

    return H

def clean_contracted(G, green_node, red_node):
    """
    if node are black and not a meta it's probably a mistake.
    we remove them and re run contraction
    we probably need make a better check in future
    """

    for n in set(G.nodes()) - (green_node.union(red_node)):
        if "_" not in n:
            G.remove_node(n)

    return contract_linear_path(G)

def simplify_meta_node_name(G):
    relabeling = dict()

    for i, n in enumerate(list(G.nodes())):
        if "_" in n:
            node_name = f"meta_node_{i}"
            relabeling[n] = node_name

    nx.relabel_nodes(G, relabeling, copy=False)

def color_graph(G):

    green_node = set()
    red_node = set()
    black_node = set()

    for node in G.nodes():
        if len(set(G.successors(node))) == 1 and len(set(G.predecessors(node))) == 0:
            black_node.add(node)

        if len(set(G.successors(node))) == 0 and len(set(G.predecessors(node))) == 1:
            black_node.add(node)

        if len(set(G.successors(node))) >= 2:
            red_node.add(node)
    
        if len(set(G.predecessors(node))) >= 2:
            green_node.add(node)

    return green_node, red_node, black_node

class JonctionType(Enum):
    SIMPLE = "simple"
    REPETITION = "repetition"
    HETEROZYGOSI = "heterozygosi"
    PROB_REPETITION = "prob_repetition"
    PROB_HETEROZYGOSI = "prob_heterozygosi"
    NOT_STUDY = "not_study"

def is_repetition(G, paths, begin, end):
    if (begin in paths and end in paths[begin]) and (end in paths and begin in paths[end]):
        return True

    return False

def is_heterozigosi(G, paths, begin, end):
    H = G.copy()

    if begin in paths and end in paths[begin]:
        H.remove_nodes_from(paths[begin][end][1:-1])
        if nx.has_path(H, begin, end):
            return True

    return False

def is_prob_repetition(G, paths, red, blacks):
    blacks = list(blacks)

    counter = 0
    if len(blacks) < 3:
        return counter

    for b in zip(blacks, blacks[1:], blacks[2:]):
        for b1, b2, b3 in permutations(b):
            if (b1 in paths and red in paths[b1]) \
                    and (b2 in paths and red in paths[b2]) \
                    and (red in paths and b3 in paths[red]):
                counter += 1

    return counter

def is_prob_heterozigosi(G, paths, green, blacks):
    blacks = list(blacks)

    counter = 0
    if len(blacks) < 3:
        return counter

    for b in zip(blacks, blacks[1:], blacks[2:]):
        for b1, b2, b3 in permutations(b):
            if (b1 in paths and green in paths[b1]) \
                    and (green in paths and b2 in paths[green]) \
                    and (green in paths and b3 in paths[green]):
                counter += 1

    return counter

