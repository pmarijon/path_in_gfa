#!/usr/bin/env python3

import path_in_gfa

from path_in_gfa.utils import *

import gfapy

from itertools import product, combinations
from collections import defaultdict, Counter
import networkx as nx

import argparse
import logging

import sys
import csv
import os

from pathlib import Path

import multiprocessing as mp

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(levelname)s :: %(message)s")
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)

def search_shortest_path(G, read_a, read_b):
    
    try:
        l = len(nx.shortest_path(G, read_a, read_b))
    except nx.exception.NetworkXError as e:
        logging.error("find good read "+str(e))
        l = float("inf")
    except nx.exception.NetworkXNoPath as e:
        logging.error("find good read "+str(e))
        l = float("inf")
    except nx.exception.NodeNotFound as e:
        logging.error("find good read "+str(e))
        l = float("inf")
   
    return (read_a, read_b), l

def cool_layout(G, name, first_node, second_node, edge_of_path, canu2realname, read2tig, tig2color): 
    if not G or G.number_of_edges() > 1000:
        logging.critical("We didn't generate svg for {} path, subgraph is too large.".format((first_node, second_node)))
        open(name + ".svg", "w").write(path_in_gfa.no_output)
        return

    A = nx.drawing.nx_agraph.to_agraph(G)

    A.graph_attr['margin'] = '0'
    A.graph_attr['rankdir'] = 'LR'
    
    A.node_attr['shape'] = 'box'
    A.node_attr['style'] = 'rounded, filled'
    A.node_attr['fontname'] = 'sans'
    A.node_attr['fontsize'] = '10'
    A.node_attr['penwidth'] = 2
    
    A.edge_attr['penwidth'] = 2
    A.edge_attr['color'] = 'grey'

    # if variable is set we assign color
    if canu2realname and read2tig and tig2color:
        for node in A.iternodes():
            if node[:-1] in canu2realname:
                node_name = canu2realname[node[:-1]]
                if node_name in read2tig and read2tig[node_name] in tig2color:
                    color = tig2color[read2tig[node_name]]
                    if color != "#000000FF":
                        node.attr["color"] = color
    else:
        for node in A.iternodes():
            node.attr["color"] = r"#888888"
    
    for node in A.iternodes():
        if not G.has_node(node):
            continue
        if nx.get_node_attributes(G, "containment")[node]:
            if node.attr["color"] != "":
                node.attr["color"] = r"".join([node.attr["color"], "88"])
            else:
                node.attr["color"] = r"#d3d3d388"
        if node == first_node:
            node.attr["shape"] = 'rarrow'
        if node == second_node:
            node.attr["shape"] = 'larrow'
    
    for edge in A.iteredges():
        if G.get_edge_data(*edge)["overhang_maplen"] > 1:
            edge.attr["style"] = u"dotted"
        edge.attr["label"] = u" ".join([u" ".join((a, v)) for a, v in edge.attr.iteritems() if a == "len"])

    for edge in edge_of_path:
        if edge in G.edges:
            A.get_edge(*edge).attr["color"] = u"#0000FF"

    try:
        A.write(name + ".dot")
    except IOError as e: 
        logging.critical("We can't generate dot for {} path (probably a graphviz's segfault).".format((first_node, second_node)))
        open(name + ".svg", "w").write(path_in_gfa.no_output)
        return #If whe failled to generate dot didn't try to generate svg
    try:
        A.draw(name + ".svg", prog='dot')
    except IOError as e:
        logging.critical("We can't generate svg for {} path (probably a dot's segfault).".format((first_node, second_node)))
        open(name + ".svg", "w").write(path_in_gfa.no_output)

def find_junction_type(G):
    
    if not G or G.number_of_edges() > 1000:
        logging.critical("We didn't try to understand this jonction is to large sorry.")
        return {JonctionType.NOT_STUDY: 1}, G

    if len(G) == 0:
        return {JonctionType.NOT_STUDY: 1}, G

    H = contract_linear_path(G)
    neighbour_transitive_reduction(H)
    H = contract_linear_path(H)
    #H = clean_contracted(H, *color_graph(H))
    
    simplify_meta_node_name(H)

    green_node, red_node, black_node = color_graph(H)

    paths = dict(nx.all_pairs_shortest_path(H))

    if len(H) <= 1:
        return {JonctionType.SIMPLE: 1}, H

    answer = Counter()
    for green, red in product(green_node, red_node):
        if is_repetition(H, paths, red, green):
            answer[JonctionType.REPETITION] += 1
        if is_heterozigosi(H, paths, red, green):
            answer[JonctionType.HETEROZYGOSI] += 1
    
    for red in red_node:
        answer[JonctionType.PROB_REPETITION] += \
            is_prob_repetition(G, paths, red, black_node)
        
    for green in green_node:
        answer[JonctionType.PROB_HETEROZYGOSI] += \
            is_prob_heterozigosi(G, paths, green, black_node)

    return answer, H


def extract_graph(param):
    G, canu2realname, read2tig, tig2color, tig2nb_read, row = param
    prefix, path, path_len, row = row
    
    
    #securise read name
    readA = row["readA"].replace("/", "")
    readB = row["readB"].replace("/", "")
    filename = prefix+"_"+row["tigA"]+"_"+readA+"_"+row["tigB"]+"_"+readB

    node_set, edge_of_path = generate_subgraph_union(row["readA"], row["readB"], G, path_len * 2)
    union = G.subgraph(node_set).copy()
    
    if union is None:
        union = nx.DiGraph()
 
    nx.write_graphml(union, filename + ".graphml")
   
    cool_layout(union, filename, row["readA"], row["readB"], edge_of_path, canu2realname, read2tig, tig2color)
    
    junction_type, contracted_G = find_junction_type(union)
    
    del union

    junction_type = "_".join(["{}-{}".format(k.value, v) for k, v in junction_type.items()])
    
    if len(junction_type) == 0:
        junction_type = JonctionType.NOT_STUDY.value

    nx.write_graphml(contracted_G, filename + ".contracted.graphml")

    contig = defaultdict(int)
    for i in {n for edge in edge_of_path for n in edge}:
        try :
            contig[read2tig[canu2realname[i[:-1]]]] += 1
        except:
            pass

    nb_read_path = ";".join(["{}:{}/{}".format(name, n, tig2nb_read[name]) for name, n in contig.items()])

    if path:
        return [row["tigA"], row["readA"], row["tigB"], row["readB"]] + [path, path_len, junction_type, filename + ".svg", nb_read_path, node_set]
    else :
        return [row["tigA"], row["readA"], row["tigB"], row["readB"]] + [path, 0, junction_type, filename + ".svg", nb_read_path, node_set]


def neighbour_transitive_reduction(G):
    
    remove_edge = set()
    for x in G.nodes():
        for y in G.successors(x):
            if (x, y) in remove_edge:
                continue

            for z in (set(G.successors(x)) & set(G.successors(y))):
                if (y, z) in remove_edge or (x, z) in remove_edge:
                    continue

                remove_edge.add((x, z))

    for n1, n2 in remove_edge:
        G.remove_edge(n1, n2)

    return G

def read_canu2realname(canu2realname):
    ret = dict()

    with open(canu2realname) as fd:
        reader = csv.reader(fd, delimiter="\t")
        for row in reader:
            ret[row[1].split(" ")[0]] = row[0]

    return ret

def read_read2contig(read2contig):
    ret = dict()
    tig2nb_read = Counter()
    
    with open(read2contig) as fd:
        reader = csv.reader(fd, delimiter="\t")
        next(reader) # eliminate first line
        for row in reader:
            ret[row[0]] = row[1]
            tig2nb_read[row[1]] += 1

    rm_tig = {i for i in tig2nb_read if tig2nb_read[i] <= 1}

    for k, v in list(ret.items()):
        if v in rm_tig:
            del ret[k]

    return ret, tig2nb_read

def read_contig2color(contig2color):
    ret = dict()

    with open(contig2color) as fd:
        reader = csv.reader(fd, delimiter=",")
        next(reader) # eliminate first line
        for row in reader:
            ret[row[0]] = row[1]

    return ret

def check_trans_red(G):
    nb_error = 0
    for x in G.nodes():
        for y in G.successors(x):
            for z in (set(G.successors(x)) & set(G.successors(y))):
                nb_error += 1

    return nb_error


def main(args = None):

    if args is None:
        args = sys.argv[1:]

    parser = argparse.ArgumentParser(prog="paf2gfa",
                                     formatter_class=argparse.
                                     ArgumentDefaultsHelpFormatter)

    parser.add_argument("gfa", type=argparse.FileType('r'), help="gfa file")
    parser.add_argument("search_path", help="path to csv contains extremity")
    parser.add_argument("result_path", help="path to csv contains result of extremity search")
    parser.add_argument("prefix", help="prefix for output file", default="")
    parser.add_argument("-t", "--thread", help="number of thread usable, 0 or minus automaticly set to number of cpu", default=1, type=int)
    parser.add_argument("-n", "--canu2realname", help="path of canu gkpStore readName.txt file. If read2contig or contig2color isn't set this option ar ignore")
    parser.add_argument("-r", "--read2contig", help="path of readToTig canu file. If contig-color isn't set this option are ignore")
    parser.add_argument("-c", "--contig2color", help="path to legend file generate by assembly_report graph projection. If read2contig isn't set this option are ignore")

    arg = vars(parser.parse_args(args))
    gfa = arg["gfa"].name
    prefix = arg["prefix"]

    logging.debug("begin parseGFA")
    graph = gfapy.Gfa.from_file(gfa)
    logging.debug("end parseGFA")
    G = nx.DiGraph()
    logging.debug("populate graph begin")
    populate_graph(graph, G)
    logging.debug("populate graph end")

    logging.debug("begin transitiv reduction")
    logging.debug("nb edge : " + str(G.number_of_edges()))
    logging.debug("nb node : " + str(G.number_of_nodes()))
    logging.debug("nb strong components : " + str(nx.number_strongly_connected_components(G)))
    logging.debug("nb weak components : " + str(nx.number_weakly_connected_components(G)))
   
    G = transitive_reduction(G)
    
    nb_trans = check_trans_red(G)
    if nb_trans > 0:
        logging.warning("Nb error in transitive reduction "+str(nb_trans))

    logging.debug("nb edge : " + str(G.number_of_edges()))
    logging.debug("nb node : " + str(G.number_of_nodes()))
    logging.debug("nb strong components : " + str(nx.number_strongly_connected_components(G)))
    logging.debug("nb weak components : " + str(nx.number_weakly_connected_components(G)))
    logging.debug("end transitiv reduction")

    canu2realname = None
    read2contig = None
    contig2color = None
    if arg["canu2realname"] and arg["read2contig"] and arg["contig2color"]:
        logging.debug("begin read aditional info")
        canu2realname = read_canu2realname(arg["canu2realname"])
        read2contig, tig2nb_read = read_read2contig(arg["read2contig"])
        contig2color = read_contig2color(arg["contig2color"])
             
        logging.debug("end read aditional info")

    contig2contig = "/".join(Path(arg["gfa"].name).parts[:-2]) + "/contig2contig.lst"
    taxonomic_assign = "/".join(Path(arg["gfa"].name).parts[:-2]) + "/taxonomic_assign.lst"
        
    delete_contig = set()
    if os.path.isfile(contig2contig) and os.path.isfile(taxonomic_assign):
        logging.debug("reading of contig categorisation")
        with open(contig2contig) as fh:
            for line in fh:
                delete_contig.add(line.strip()[3:].lstrip("0"))
        
        with open(taxonomic_assign) as fh:
            for line in fh:
                line = line.strip().split(";")
                if line[1] != "genomic":
                    delete_contig.add(line[0][3:].lstrip("0"))

        logging.debug("end reading of contig categorisation")

    ext2ext = dict()
    with open(arg["search_path"], "r") as search:
        reader = csv.DictReader(search)
        for row in reader:
            if row["tigA"] in delete_contig or row["tigB"] in delete_contig:
                logging.debug("skip this {} {}".format(row["tigA"], row["tigB"]))
                continue
            
            (read_a, read_b), l = search_shortest_path(G, row["readA"], row["readB"])

            row["readA"] = read_a
            row["readB"] = read_b
            row["length"] = l

            if (row["readA"], row["readB"]) in ext2ext:
                if l < ext2ext[(row["readA"], row["readB"])]["length"]:
                    ex2ext[(row["readA"], row["readB"])] = row
            else:
                ext2ext[(row["readA"], row["readB"])] = row

    pool = mp.Pool(arg["thread"])
    async_res = pool.map_async(extract_graph, ((G,
                                                canu2realname,
                                                read2contig,
                                                contig2color,
                                                tig2nb_read,
                                                (prefix, True,
                                                 row["length"], row)) \
                                               for row in ext2ext.values() \
                                               if row["length"] != float("inf"))
                               )
    res = async_res.get()
    all_nodes = set()
    with open(arg["result_path"], "w") as result:
        writer = csv.writer(result)
        writer.writerow(["tigA", "readA", "tigB", "readB", "path_find", "path_len", "repetition", "image_path", "nb_contig"])

        for row in res:
            *row, node_set = row
            all_nodes.update(node_set)
            writer.writerow(row)

    nx.write_graphml(G.subgraph(all_nodes), prefix+"_all_junction.graphml")

if __name__ == "__main__":
    main(sys.argv[1:])
