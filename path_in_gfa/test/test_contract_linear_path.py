import pytest

import networkx as nx
import path_in_gfa.utils

@pytest.fixture
def graph():
    G = nx.DiGraph()

    G.add_edge("a", "b")
    G.add_edge("b", "c")
    G.add_edge("c", "d")
    G.add_edge("d", "e")
    G.add_edge("e", "f")
    G.add_edge("f", "g")
    G.add_edge("g", "h")
    
    G.add_edge("h", "c")
    G.add_edge("e", "i")

    return G

@pytest.fixture
def graph_linear():
    G = nx.DiGraph()

    G.add_edge("a", "b")
    G.add_edge("b", "c")
    G.add_edge("c", "d")
    G.add_edge("d", "e")
    G.add_edge("e", "f")
    G.add_edge("f", "g")
    G.add_edge("g", "h")
    
    return G

def test_linear_paths(graph):

    true = [['a', 'b'], ['d'], ['f', 'g', 'h'], ['i']]
    for i, path in enumerate(path_in_gfa.utils.linear_paths(graph)):
        assert true[i] == path


def test_contract_linear_path(graph):

    true = {'nodes': ('c', 'd', 'e', 'i', 'a_b', 'f_g_h'),
            'edges': (('c', 'd'), ('d', 'e'), ('e', 'i'), ('e', 'f_g_h'),
                      ('a_b', 'c'), ('f_g_h', 'c'))
            }

    H = path_in_gfa.utils.contract_linear_path(graph)

    assert tuple(H.nodes()) == true['nodes']
    assert tuple(H.edges()) == true['edges']


def test_contract_linear_path_on_linear_graph(graph_linear):

    true = {'nodes': ('a_b_c_d_e_f_g_h',),
            'edges': ()
            }

    H = path_in_gfa.utils.contract_linear_path(graph_linear)

    assert tuple(H.nodes()) == true['nodes']
    assert tuple(H.edges()) == true['edges'] 
