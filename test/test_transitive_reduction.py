import sys
sys.path.append('../')
import path_in_gfa.utils
import path_in_gfa.extract_dag
import networkx
import gfapy

# Read graph for file or construct from gfa
graph = gfapy.Gfa.from_file(sys.argv[1])
    
G = networkx.DiGraph()
path_in_gfa.utils.populate_graph(graph, G)

import networkx

original_G = G.copy()
myers_G = G.copy()
topo_G = G.copy()

path_in_gfa.utils.transitive_reduction(myers_G)
path_in_gfa.extract_dag.neighbour_transitive_reduction(topo_G)

networkx.write_graphml(original_G, sys.argv[1]+".original.graphml")
networkx.write_graphml(myers_G, sys.argv[1]+".myers.graphml")
networkx.write_graphml(topo_G, sys.argv[1]+".topo.graphml")

import imp
try:
    imp.find_module('asciinet')
except ImportError:
    exit("to print the graph, install asciinet: https://github.com/cosminbasca/asciinet")


from asciinet import graph_to_ascii
print("before transitive reduction")
print(graph_to_ascii(original_G))
print("myers transitive reduction")
print(graph_to_ascii(myers_G))
print("topo transitive reduction")
print(graph_to_ascii(topo_G))
